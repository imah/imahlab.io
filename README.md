Un projet de Raytracing par Hector Denis.

Le code est dans le fichier **main.c**

et se compile avec la commande **make** et le fichier **makefile** associé.

Il faut aussi
https://github.com/nothings/stb
pour satisfaire
#include "stb_image_write.h"

[Example plain HTML site using GitLab Pages: https://pages.gitlab.io/plain-html]
