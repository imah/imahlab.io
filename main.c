// vim: foldmethod=syntax:foldnestmax=1:foldlevel=1:foldclose=all
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
// defining NDEBUG disables assert()
//#define NDEBUG
#include <assert.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

//https://stackoverflow.com/questions/21007329/what-is-an-sdl-renderer
#include <SDL.h>
#include <SDL_image.h>

#define FNAME "kimi.png"
typedef struct scene scene;
struct scene {
	int nsph;
	double (*sph)[][3];
	double (*rad)[];
	double (*kd)[][3];
	double (*kr)[][3];

	int nsrc;
	double (*src)[][3];
	double (*col)[][3];
	int *svis;

	int n;
	int delta;
	int rmax;
	double* om;
	double* bg;
};

#define ACCESS(a,n,p,i,j,k) ( (a) [(n)*(p)*(i) + (p)*(j) + (k)] )

double vz[3] = {0., 0., 0.};
void vfma(double* a, double z, double* b, double* c) {
	c[0] = a[0] + b[0] * z;
	c[1] = a[1] + b[1] * z;
	c[2] = a[2] + b[2] * z;
}
int vprint(double*a) { return printf("%lf %lf %lf", a[0], a[1], a[2]); }
void cpvto(double*from, double*to) {to[0]=from[0]; to[1]=from[1]; to[2]=from[2];}
void vec(double* a, double* b, double* c) { vfma(b, -1, a, c); }
double ps(double* a, double* b) { return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]; }
double norme(double* a) { return sqrt(ps(a,a)); }
void unitaire(double* a, double* u) { vfma(vz, 1/norme(a), a, u); }
void pt(double* a, double* u, double t, double* p) { vfma(a, t, u, p); }
void dir(double*a, double*b, double* c) { vec(a,b,c); unitaire(c,c); }
void c2(double*a, uint8_t*c) {
	c[0] = (uint8_t) (255*a[0]);
	c[1] = (uint8_t) (255*a[1]);
	c[2] = (uint8_t) (255*a[2]);
}

double intersection(double*a,double*u,double*c,double rad) {
	double ca[3] = {0};
	vec(c,a,ca);
	double x = ps(u,ca);
	double y = rad;
	double z = norme(ca);

	double d = x*x + y*y - z*z;
	if (d<0) return -1;
	double t1 = -x + sqrt(d);
	double t2 = -x - sqrt(d);
	if (t2 >= 0) return t2;
	if (t1 >= 0) return t1;
	return -1;
}

int au_dessus(double*c,double*p,double*src) {
	double x[3] = {0};
	double y[3] = {0};
	vec(p,c,x);
	vec(p,src,y);
	return ps(x,y) <= 0;
}

int visible(int nb_sph, double spheres[][3],double*radius,int j,double*p,double*src) {
	double v[3] = {0};
	double u[3] = {0};
	double d = 0;
	vec(p,src,v);
	unitaire(v,u);
	d = norme(v);

	if (!(au_dessus(spheres[j],p,src))) return 0;

	for (int i = 0; i<nb_sph; i++) {
		if (i == j) continue;
		double t = intersection(p,u,spheres[i],radius[i]);
		if (t < 0) continue;
		if (t < d) return 0;
	}
	return 1;
}

void couleur_diffusee(double*u, double*colsrc, double*n, double*kd,double*cd) {
	double um[3] = {0};
	double nunit[3] = {0};
	vfma(vz,-1,u,um);
	unitaire(n,nunit);
	double th = ps(nunit, um);
	//double cth = cos(th);
	//th = cos(th);
	cd[0] = colsrc[0] * kd[0] * th;
	cd[1] = colsrc[1] * kd[1] * th;
	cd[2] = colsrc[2] * kd[2] * th;
}

//impact de u<calcule> sur la sphere (c[DONNE],rad) au point p[DONNE] emis depuis src[DONNE], et rend w[Descartes]
//src doit etre visible depuis p
void rayon_reflechi(double*c,double*p,double*src,double* w) {
	double u[3] = {0};
	dir(src, p, u);

	double um[3] = {0};
	vfma(vz, -1, u, um);

	double nv[3] = {0};
	dir(c,p,nv);

	//double x = ps(nv,um);
	//int b = (fabs(x) <= 1);
	//assert(b); //probablement un ebonne idee de l'avoir
	//assert( ps(nv, um) >= 0 );

	double h[3] = {0};
	vfma(vz, ps(nv,um), nv, h);
	double z[3] = {0};
	vfma(u, 1, h, z);

	vfma(um, 2, z, w);
	unitaire(w,w);
}

void grille(int i, int j, int N, double d, double* p) {
	double dd2 = d / 2;
	double mx = ((i * d) / N) - dd2;
	double my = ((-j * d) / N) + dd2;
	p[0] = mx; p[1] = my; p[2] = 0.;
}
void rayon_ecran(double* om, int i, int j, int N, double d, double*r) {
	double p[3] = {0};
	grille(i,j,N,d,p);
	dir(om,p,r);
}

//le rayon(a,u) est intercepte en sphere(idx), au point p
void interception(int nb_sph, double spheres[][3], double*radius,double*a,double*u,double*p,int*idx) {
	double d = INFINITY; 
	int j = -1;
	for (int i = 0; i<nb_sph; i++) {
		double t = intersection(a,u,spheres[i],radius[i]);
		// 6heures de temps, un amour perdu, la vie continue
		//shadow acne
		if (t < 0.001) continue; 
		if (t < d) {
			d = t;
			j = i;
		}
	}
	*idx = j;
	if (-1 == j) return;
	pt(a,u,d,p);
}

double clipscalar(double x, double min, double max) {
	if (x < min) return min;
	if (x > max) return max;
	return x;
}
void cliparray(double* a, double min, double max) {
	a[0] = clipscalar(a[0], min, max);
	a[1] = clipscalar(a[1], min, max);
	a[2] = clipscalar(a[2], min, max);
}

void couleur_diffusion(scene* s, double*p, int j, double* cd) {
	double* kd = (*s->kd)[j];
	double* c = (*s->sph)[j];
	double nv[3] = {0};
	dir(c, p, nv);
	cd[0] = cd[1] = cd[2] = 0.;

	for (int i = 0; i < s->nsrc; i++) {
		if (0 == s->svis[i]) continue;
		if (0 == visible(s->nsph, *s->sph, *s->rad, j, p, (*s->src)[i])) continue;
		double* cs = (*s->col)[i];
		double u[3] = {0};
		vec((*s->src)[i], p, u);
		unitaire(u,u);
		double cdtmp[3] = {0};

		couleur_diffusee(u, cs, nv, kd, cdtmp);
		vfma(cd, 1, cdtmp, cd); //on additionne
	}
	cliparray(cd, 0., 1.);
}


void rt2int(double* im, uint8_t* xy, int n) {
#define IMG(i,j,k) ACCESS(im, n, 3, i,j,k)
#define XYZ(i,j,k) ACCESS(xy, n, 3, i,j,k)

	for (int j = 0; j < n; j++) {
		for (int i = 0; i < n; i++) {
			c2(&IMG(i,j,0), &XYZ(j,i,0));
		}
	}
}

void drawppm(uint8_t* xy, int n) { //redirect with ./exe > a.ppm
#undef XYZ
#define XYZ(i,j,k) ACCESS(xy, n, 3, i,j,k)
	fprintf(stdout, "P3\n%d %d\n255\n", n, n);
	for (int j = 0; j < n; j++) {
		for (int i = 0; i < n; i++) {
			fprintf(stdout, "%d %d %d\n", XYZ(j,i,0), XYZ(j,i,1), XYZ(j,i,2));
		}
	}
}

void drawpng(uint8_t* xy, int n, const char* filename) {
	stbi_write_png(filename, n,n,3,xy,3*n);
}

void lancer(double*im, scene*s) {
#undef IMG
#define IMG(i,j,k) ACCESS(im, s->n, 3, i,j,k)

	double ray[3] = {0};

	for (int i = 0; i < s->n; i++) {
		//fprintf(stderr, "%d\r", i);
		for (int j = 0; j < s->n; j++) {
			IMG(i,j,0) = s->bg[0];
			IMG(i,j,1) = s->bg[1];
			IMG(i,j,2) = s->bg[2];

			rayon_ecran(s->om,i,j,s->n,s->delta,ray);

			double p[3] = {0};
			int k = 0;
			interception(s->nsph,*s->sph,*s->rad,s->om,ray,p,&k);
			if (-1 == k) continue;
			couleur_diffusion(s,p,k, &IMG(i,j,0));
		}
	}
}

//void reflexions(int nsph, double lsph[][3], double*lrad, double*a, double*u, int rmax, double pts[][3], int* lidx, int* rxlen) {
void reflexions(scene* s, double*a, double*u, double pts[][3], int*lidx, int* rxlen) {
	double v[3] = {0};
	cpvto(u,v);
	double b[3] = {0};
	cpvto(a,b);

	int i = 0;
	while (i < s->rmax) {
		double p[3] = {0};
		int ids = 0;
		interception(s->nsph,*s->sph,*s->rad,b,v,p,&ids);

		//IF NOT VISIBLE il capte quand meme la couleur de ce point p!!!
		//if (-1 == ids || !visible(nsph,lsph,lrad,ids,p,b) ) { *rxlen = i; return; }
		if (-1 == ids) { *rxlen = i; return; }

		cpvto(p, pts[i]); //FROM TO
		lidx[i] = ids;

		//mise a jour de v, puis de b
		rayon_reflechi((*s->sph)[ids],p,b,v);
		cpvto(p,b);
		i++;
	}
	*rxlen = i;
}

#define RMAX 10
double pts[RMAX][3] = {{0}};
int idx[RMAX] = {0};

void couleur_percue(scene* s, double*a, double* u, double* cf) {

	int len = -1;

	reflexions(s,a,u,pts,idx,&len);
	assert(len >= 0);

	if (0 == len) {
		cpvto(s->bg, cf);
		return;
	}

	cpvto(vz,cf); //on part d'une couleur nulle

	for (int i = 0; i < len; i++) {
		int j = len - 1 - i;
		int k = idx[j];
		double* kr = (*s->kr)[k];

		double cd[3] = {0};
		couleur_diffusion(s,pts[j],k,cd);
		cf[0] = cd[0] + kr[0] * cf[0];
		cf[1] = cd[1] + kr[1] * cf[1];
		cf[2] = cd[2] + kr[2] * cf[2];
	}
	cliparray(cf, 0., 1.); //a enlever pour des effets bizarres [qui n'existent plus...]
}

void lancer_complet(double* im, scene* s) {
#undef IMG
#define IMG(i,j,k) ACCESS(im, s->n, 3, i,j,k)

	double ray[3] = {0};
	for (int i = 0; i < s->n; i++) {
		for (int j = 0; j < s->n; j++) {
			rayon_ecran(s->om,i,j,s->n,s->delta, ray);
			couleur_percue(s, s->om, ray, &IMG(i,j,0));
		}
	}
}


typedef struct Input Input;
struct Input {
	int key[SDL_NUM_SCANCODES];
	int repeat[SDL_NUM_SCANCODES];
	int mousex;
	int mousey;
	int mousexrel;
	int mouseyrel;
	int mousebuttons[8];
	int quit;
};

void updateEvents(Input *in) {
	SDL_Event event;
	//printf("avant\t");
	SDL_WaitEvent(&event);
	//printf("apres\n");
	//on attend un event, on remplt input puis on continue a la boucle principale
	//while(SDL_WaitEvent(&event))
	{
		switch (event.type) {
			case SDL_WINDOWEVENT:// check window id cf wiki
				if (event.window.event == SDL_WINDOWEVENT_CLOSE) in->quit = 1;
				break;
			case SDL_KEYDOWN:
				//printf("scan,rep:%d,%d\n", event.key.keysym.scancode, event.key.repeat);
				in->key[event.key.keysym.scancode] = 1;
				in->repeat[event.key.keysym.scancode] = event.key.repeat;
				break;

			case SDL_KEYUP:
				in->key[event.key.keysym.scancode] = 0;
				break;

			case SDL_MOUSEMOTION:
				in->mousex=event.motion.x;
				in->mousey=event.motion.y;
				in->mousexrel=event.motion.xrel;
				in->mouseyrel=event.motion.yrel;
				break;

			case SDL_MOUSEBUTTONDOWN:
				in->mousebuttons[event.button.button] = 1;
				break;

			case SDL_MOUSEBUTTONUP:
				in->mousebuttons[event.button.button] = 0;
				break;

			case SDL_QUIT:
				in->quit = 1;
				break;
			default:
				break;
		}
	}
}

void drawsave(void) { system("cp "FNAME" ims/im-$(date +%Y-%m-%d--%H-%M-%S).png"); }

#define f0(x) (sizeof((x)) / sizeof((x)[0]))
int main(void) {
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_TIMER);
	IMG_Init(IMG_INIT_PNG);

	scene sc;
	memset(&sc, 0, sizeof(sc));
#if 0
	double lsph[][3] = {{10.,0.,0.}, {-10., 0., 0.}, {20.,-4.,-20.}, {-15.,-30.,-20.}, {0.,-10.,20.}, {0., 10., -5.}};
	double lrad[] = {3., 3., 4., 5.,2., 3.};

	double lkd[][3] = {{0., .5, 1.}, {1.,0.,0.}, {1.,0.58,0.156}, {.4,.4,0.}, {.2,0.,.2}, {1.,1.,1.}};
	double lkr[][3] = {{.2,.6,.7},{.8,.1,.9},{.0,.5,.2},{.3,.3,.3}, {1.,1.,1.}, {0.,0.,0.}};
	//double lkr[][3] = {{.0,.0,.0},{.0,.0,.0},{.0,.0,.0},{.0,.0,.0},{0.,0.,0.}};

	double lsrc[][3] = {{-10.,0.,50.}, {10., -20., 50.}, {0., -20., 50.}};
	double lcol[][3] = {{1.,1.,1.}, {0.3, 0.8, 0.5}, {0.9, 0.1, 0.8}};
	int svis[] = {1,1,1};
#endif
//#if 0 
	double lsph[][3] = {{0.,-50.,0.}, {0, 3, 0.}, {10.,2.,0},
	{-10.,3.5,4}, {-4,-1.5,15.}, {0., 0., -100.}};
	double lrad[] = {47,5,4,3,2,50};

	double lkd[][3] = {{.9,.2,.2},{.4,.4,.4},{.4,0,.4},{.9,.4,.4},{0,1,1},{1,1,1}};
	double lkr[][3] = {{.2,.2,.2},{.4,.4,.4},{.4,.4,.4},{.4,.4,.4},{.6,.8,.8},{.1,.1,.1}};

	double lsrc[][3] = {{-15.,2.,30.},{20.,5.,-50.}};
	double lcol[][3] = {{1,1,1},{1,1,0}};
	int svis[] = {1,0};
//#endif
	int nsph = f0(lsph); 
	int nrad = f0(lrad); 
	int nkd = f0(lkd);
	int nkr = f0(lkr);
	assert(nsph <= nrad);
	assert(nrad == nkd);
	assert(nrad == nkr);

	int nsrc = f0(lsrc);
	int ncol = f0(lcol);
	int nsvis = f0(svis);
	assert(nsrc <= ncol);
	assert(nsvis == ncol);

	double delta = 50;
	double om[3] = {5., 10., 70.};
	double bg[3] = {0.87, 0.95, 0.99};

#define NVAL 100

	sc.nsph = nsph;
	sc.sph = &lsph;
	sc.rad = &lrad;
	sc.kd = &lkd;
	sc.kr = &lkr;

	sc.nsrc = nsrc;
	sc.src = &lsrc;
	sc.col = &lcol;
	sc.svis = svis;

	sc.n = NVAL;
	sc.delta = delta;
	sc.rmax = RMAX;
	sc.om = om;
	sc.bg = bg;

	double* im = malloc(sc.n * sc.n * 3 * sizeof(*im));
	if (NULL == im) exit(7);
	uint8_t* xy = malloc(sc.n * sc.n * 3 * sizeof(*xy));
	if (NULL == xy) exit(7);

	//todo: context structure
	int wflags = SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE;
	int rflags = SDL_RENDERER_ACCELERATED;
	SDL_Window* pw = SDL_CreateWindow("CUBIQUE", /*400*/0,0,800,800, wflags);
	SDL_Renderer* r = SDL_CreateRenderer(pw, -1, rflags);

	SDL_RaiseWindow(pw);

	SDL_Texture* t = NULL;
	Input in;
	memset(&in, 0, sizeof(in));
	int trig = 1;
	while (0 == in.quit) {
		updateEvents(&in);

		if (in.key[SDL_SCANCODE_Q] || in.key[SDL_SCANCODE_ESCAPE]) in.quit = 1;

		if(in.key[SDL_SCANCODE_LEFTBRACKET] && (in.repeat[SDL_SCANCODE_LEFTBRACKET] == 0)) {
			in.key[SDL_SCANCODE_LEFTBRACKET] = 0;
			sc.delta += 10;
			trig = 1;
		}
		if(in.key[SDL_SCANCODE_RIGHTBRACKET] && (in.repeat[SDL_SCANCODE_RIGHTBRACKET] == 0)) {
			in.key[SDL_SCANCODE_RIGHTBRACKET] = 0;
			sc.delta -= 10;
			trig = 1;
		}

		int k = SDL_SCANCODE_N;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			sc.n += 200;
			trig = 1;
			im = realloc(im, sc.n * sc.n * 3 * sizeof(*im));
			xy = realloc(xy, sc.n * sc.n * 3 * sizeof(*xy));
		}
		k = SDL_SCANCODE_M;
		if(in.key[k] && (in.repeat[k] == 0) && (sc.n - 200 > 0)) {
			in.key[k] = 0;
			sc.n -= 200;
			trig = 1;
			im = realloc(im, sc.n * sc.n * 3 * sizeof(*im));
			xy = realloc(xy, sc.n * sc.n * 3 * sizeof(*xy));
		}
		//u, hjkl, n
		int c = 10;
		k = SDL_SCANCODE_U;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			sc.om[1] += c;
			trig = 1;
		}
		k = SDL_SCANCODE_B;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			sc.om[1] -= c;
			trig = 1;
		}
		k = SDL_SCANCODE_L;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			sc.om[0] += c;
			trig = 1;
		}
		k = SDL_SCANCODE_H;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			sc.om[0] -= c;
			trig = 1;
		}
		k = SDL_SCANCODE_K;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			sc.om[2] += c;
			trig = 1;
		}
		k = SDL_SCANCODE_J;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			sc.om[2] -= c;
			trig = 1;
		}

		k = SDL_SCANCODE_S;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			drawsave();
		}

		k = SDL_SCANCODE_1;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			sc.svis[0] = 1 - sc.svis[0];
			trig = 1;
		}
		k = SDL_SCANCODE_2;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			sc.svis[1] = 1 - sc.svis[1];
			trig = 1;
		}
		k = SDL_SCANCODE_3;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			sc.svis[2] = 1 - sc.svis[2];
			trig = 1;
		}
		k = SDL_SCANCODE_4;
		if(in.key[k] && (in.repeat[k] == 0)) {
			in.key[k] = 0;
			sc.svis[3] = 1 - sc.svis[3];
			trig = 1;
		}


		if (in.key[SDL_SCANCODE_T]) { trig = 1; }
		if (trig) {
			trig = 0;
			lancer_complet(im,&sc);
			rt2int(im, xy, sc.n);
			drawpng(xy, sc.n, FNAME);

			SDL_DestroyTexture(t);
			t = IMG_LoadTexture(r, FNAME);
			SDL_SetRenderDrawColor(r, 0, 255, 0, SDL_ALPHA_OPAQUE);
			SDL_RenderClear(r);
			SDL_RenderCopy(r,t,NULL,NULL);
			SDL_RenderPresent(r);
		}

		//printf("\t event ticking... \t%d\n", SDL_GetTicks());
	}

	SDL_DestroyTexture(t);
	SDL_DestroyRenderer(r);
	SDL_DestroyWindow(pw);
	IMG_Quit();
	SDL_Quit();
	free(xy);
	free(im);
	return 0;
}
