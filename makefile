#mettre ca en postcompile2
#alias do='make && ./exe; open s.png ; cp s.png ims/im-$(date +%Y-%m-%d--%H-%m-%S).png'
#alias go='make && ./exe; cp s.png ims/im-$(date +%Y-%m-%d--%H-%m-%S).png'
#alias zo='make && ./exe;'

CC:=/usr/local/opt/llvm@12/bin/clang
#CC:=clang #for ycmgenerator (more lenient compilation)
#CFLAGS += --analyze -Xanalyzer
#CFLAGS += -analyzer-output=text
#CFLAGS += -analyzer-output=html -o .

#CC:=gcc-11
#CFLAGS += -fanalyzer #GCC only
#CFLAGS += -std=c17 -Wall -Wextra -Wuninitialized -Wshadow -march=native
CFLAGS += -std=c17 -O3
#CFLAGS += -O1 -g3 -fno-omit-frame-pointer -fsanitize=address -pedantic -fPIC 
#LINKTIME:= -D_FORTIFY_SOURCE=2

#ASAN_OPTIONS=detect_leaks=1 ./exe > a.ppm
#CFLAGS += -Wno-delete-non-abstract-non-virtual-dtor
#CFLAGS += -Wno-unused-variable -Wno-unused-parameter

SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)
OBJDIR := obj
EXE := exe

SDLLIB := -L/usr/local/lib -lSDL2 -lSDL2_image -lSDL2_ttf
SDLFLAGS := -I/usr/local/include/SDL2 -D_THREAD_SAFE
#/!\ le /SDL2 suit, donc n'est SURTOUT pas a preciser dans le code source
#SDLLIB := $(shell sdl2-config --libs)
#-L/usr/local/lib -lSDL2
#SDLFLAGS := $(shell sdl2-config --cflags)
#-I/usr/local/include/SDL2 -D_THREAD_SAFE

DEPDIR := .deps
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.Td
POSTCOMPILE = mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@
#dependencies's timestamps are newer than .o files
#so create T(emp), then move after compile, then update target == $@ == %.o

$(EXE) : $(OBJDIR) $(OBJDIR)/$(OBJ)
	$(CC) -o $(EXE) $(CFLAGS) $(LINKTIME) $(SDLLIB) $(SDLFLAGS) $(OBJDIR)/$(OBJ)

$(OBJDIR)/%.o : %.c $(DEPDIR)/%.d | $(OBJDIR) $(DEPDIR) 
	$(CC) $(DEPFLAGS) $(CFLAGS) $(SDLFLAGS) -c $< -o $@
	$(POSTCOMPILE)

$(OBJDIR) :
	@mkdir -p $@

$(DEPDIR) :
	@mkdir -p $@

DEPFILES=$(SRC:%.c=$(DEPDIR)/%.d)
$(DEPFILES) :

include $(wildcard $(DEPFILES))

.PHONY : clean
clean :
	rm -f $(EXE)
	rm -rf $(OBJDIR)
	rm -rf $(DEPDIR)
	#rm -rf .clangd

#/usr/local/opt/llvm@11/bin
print-% : ; @echo $*=$($*) ;
